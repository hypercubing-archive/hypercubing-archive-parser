from bs4 import BeautifulSoup
from email.message import EmailMessage
from html import unescape

from .util import escape


class ArchiveMessageTreeNode:
    """Holds a single node of the ArchiveMessageTree.

    It contains the entire email message as well as its child parts.
    The entire message already contains the child parts, but this tree
    stores only the children relevant to us.
    """

    def __init__(self, value: EmailMessage, children) -> None:
        self.value = value
        self.children = children


class ArchiveMessageTree:
    """Holds the useful subparts that make up an email message.

    email.message.EmailMessage is in the form of a tree, because an email
    message sometimes contains multiple parts, with parts that could
    contain their own subparts.

    This class also provides a methods to serialize the message tree into
    a single string.
    """

    def __init__(self, ref_id: int, parsed: EmailMessage) -> None:
        self._ref_id = ref_id
        self._header = parsed  # Just ref the entire message for now
        self._tree = self._build_tree(parsed)

    def _build_tree(self, parsed: EmailMessage) -> ArchiveMessageTreeNode:
        """Builds the tree from the email message.

        The tree is essentially the email message (which is actually itself
        a tree) but with unwanted sections excluded.
        """
        # It's not multipart, so return the node with no children
        if not parsed.is_multipart():
            return ArchiveMessageTreeNode(parsed, [])

        # Alternative means there's multiple representations of the same
        # text, so choose the first one
        if (
            parsed.is_multipart()
            and parsed.get_content_subtype() == "alternative"
        ):
            return ArchiveMessageTreeNode(
                next(parsed.iter_parts()), []  # type: ignore
            )

        # We actually have multiple parts.
        children = []
        for node in parsed.iter_parts():
            content_disposition = node.get_content_disposition()
            # Ignore attachments and 'application/x-ygp-stripped' content.
            if content_disposition and "attachment" in content_disposition:
                continue
            if node.get_content_type() == "application/x-ygp-stripped":
                continue
            children.append(self._build_tree(node))  # type:ignore
        return ArchiveMessageTreeNode(parsed, children)

    def _process_html(self, segment: str) -> str:
        """Removes html tags from the message and unescapes some characters."""
        segment = segment.replace("<br>", "\n")
        soup = BeautifulSoup(segment, "html.parser")
        return unescape(soup.get_text())

    def _serialize_node(self, node: ArchiveMessageTreeNode) -> str:
        """Serializes a single node/message subpart into a string.

        The subpart could be HTML or plaintext. For HTML, we remove the tags
        and unescape some characters to reduce noise.
        """
        content_type = node.value.get_content_type()
        content = node.value.get_content()
        message = ""
        if content_type == "text/html":
            message = self._process_html(content)
        elif type(content) == str:
            message = content
        else:
            raise Exception("Invalid content type " + content_type)
        return message.strip() + "\n\n"

    def _serialize_nodes(self, node: ArchiveMessageTreeNode) -> str:
        if len(node.children):
            serialized = ""
            for child in node.children:
                serialized += self._serialize_node(child)
        else:
            serialized = self._serialize_node(node)
        return serialized

    def _serialize_headers(self, message: EmailMessage) -> str:
        serialized = f'From: {message["From"]}\n'
        serialized += f'Subject: {message["Subject"]}\n'
        serialized += f'Date: {message["Date"]}\n'
        return serialized

    def serialize(self) -> str:
        """Serializes the entire message to a string."""
        serialized_header = self._serialize_headers(self._header)
        serialized_body = self._serialize_nodes(self._tree)
        content = escape(f"{serialized_header}\n{serialized_body}")
        # Replace newline with br but replace double-br with newlines
        # because of the way Markdown works
        content = content.replace("\n", "<br>\n").replace(
            "<br>\n<br>\n", "\n\n"
        )
        return content

    def get_id(self) -> int:
        return self._ref_id

    def get_header(self) -> EmailMessage:
        return self._header
