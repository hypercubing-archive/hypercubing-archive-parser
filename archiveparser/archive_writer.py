from .archive_message_tree import ArchiveMessageTree
from .thread_forest import ThreadForest, ThreadForestNode
from .util import escape, pad_id


class Post:
    def __init__(self, ref_id: int, content: str) -> None:
        self._ref_id = ref_id
        self._content = content

    def get_id(self) -> int:
        return self._ref_id

    def get_content(self) -> str:
        return self._content


class Archive:
    """Represents the archive file page contents.

    - index_page is a string of Markdown containing the list of threads.
    - posts is the list of messages posted to the archive.
    """

    def __init__(self, index_page: str, posts: list[Post]) -> None:
        self.index_page = index_page
        self.posts = posts


class ArchiveWriter:
    """Generates all the pages of the archive.

    Uses both the messages and the thread forest
    to generate all of the Markdown content.
    """

    def __init__(
        self, messages: list[ArchiveMessageTree], thread_forest: ThreadForest
    ) -> None:
        self._messages = messages
        self._thread_forest = thread_forest
        # Upgrade the messages to be indexed by their ID
        self._indexed_messages = self._make_index(messages)

    def _make_index(
        self, messages: list[ArchiveMessageTree]
    ) -> dict[int, ArchiveMessageTree]:
        indexed_messages = dict()
        for message in messages:
            indexed_messages[message.get_id()] = message
        return indexed_messages

    def _generate_index_page(self) -> str:
        serialized = "---\n"
        serialized += "layout: threads\n"
        serialized += "---\n"
        serialized += "\n"
        serialized += self._thread_forest.serialize()
        return serialized

    def _linkify(self, ref_id: int) -> str:
        return f"{pad_id(ref_id)}.html"

    def _generate_post(self, post: ThreadForestNode) -> Post:
        message = self._indexed_messages[post.get_id()]
        serialized = "---\n"
        serialized += "layout: post\n"
        serialized += f"ref_id: {post.get_id()}\n"
        if post.prev_thread_ref_id:
            link = self._linkify(post.prev_thread_ref_id)
            serialized += f"prev_thread_id: {link}\n"
        if post.prev_post_ref_id:
            link = self._linkify(post.prev_post_ref_id)
            serialized += f"prev_post_id: {link}\n"
        if post.next_post_ref_id:
            link = self._linkify(post.next_post_ref_id)
            serialized += f"next_post_id: {link}\n"
        if post.next_thread_ref_id:
            link = self._linkify(post.next_thread_ref_id)
            serialized += f"next_thread_id: {link}\n"
        serialized += "---\n"
        serialized += "\n"
        serialized += message.serialize()
        return Post(post.get_id(), serialized)

    def _generate_posts_acc(
        self, root: ThreadForestNode, acc: list[Post]
    ) -> list[Post]:
        for child in root.children:
            acc.append(self._generate_post(child))
            if len(child.children):
                self._generate_posts_acc(child, acc)
        return acc

    def _generate_posts(self) -> list[Post]:
        return self._generate_posts_acc(self._thread_forest.get_root(), [])

    def generate_archive(self) -> Archive:
        """Generates a list of HTML pages"""
        index_page = self._generate_index_page()
        posts = self._generate_posts()
        return Archive(index_page, posts)
