def split_messages(mbox_content: bytes) -> list[bytes]:
    """Given a mailbox, splits the messages.

    We use bytes because the encodings vary throughout the file.
    """
    messages: list[bytes] = []
    current_message: bytes = b""
    split_content: list[bytes] = mbox_content.split(b"\n")
    for line in split_content:
        if line.startswith(b"From ") and b"@" in line.split(b" ")[1]:
            if len(current_message) > 0:
                messages.append(current_message)
            current_message = b""
        else:
            current_message += line + b"\n"
    messages.append(current_message)
    return messages
