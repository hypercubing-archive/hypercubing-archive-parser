from email.message import EmailMessage
from typing import Any

from .util import escape, pad_id


class ThreadForestNode:
    """A single node of the thread tree.

    It only holds the metadata of a message. It does not contain the
    message body.
    """

    def __init__(
        self,
        ref_id: int,
        children,
        from_name_email: str | None = None,
        subject: str | None = None,
        date: str | None = None,
        message_id: str | None = None,
        in_reply_to: str | None = None,
    ) -> None:
        self.ref_id = ref_id
        self.children = children
        self.from_name_email = from_name_email
        self.subject = subject
        self.date = date
        self.message_id = message_id
        self.in_reply_to = in_reply_to
        self.prev_thread_ref_id: int | None = None
        self.prev_post_ref_id: int | None = None
        self.next_post_ref_id: int | None = None
        self.next_thread_ref_id: int | None = None

    def get_id(self) -> int:
        return self.ref_id

    def to_dict(self) -> dict[str, Any]:
        return {
            "id": self.ref_id,
            "from": self.from_name_email,
            "subject": self.subject,
            "date": self.date,
            "message_id": self.message_id,
            "in_reply_to": self.in_reply_to,
            "children": [child.to_dict() for child in self.children],
        }

    def serialize(self, indent_level) -> str:
        serialized = ""
        indent = indent_level * "  "
        message_link = f"messages/{pad_id(self.ref_id)}.html"
        if self.subject is not None:
            serialized = f"{indent}- [{escape(self.subject)}]({message_link})\n"
        else:
            serialized = f"{indent}- [(No subject)]({message_link})\n"
        for child in self.children:
            serialized += child.serialize(indent_level + 1)
        return serialized


class ThreadForest:
    """The thread tree organizes the various messages into a forest.

    The tree only holds metadata on the messages. It does not contain the
    message bodies.
    A ThreadForestNode X is a child of another ThreadForestNode Y iff
    X is a reply to Y.
    """

    def __init__(self, nodes: list[ThreadForestNode]) -> None:
        self._tree = self._build_tree(nodes)

    def _link_posts_of(
        self,
        thread_root: ThreadForestNode,
        prev_thread: ThreadForestNode | None,
        next_thread: ThreadForestNode | None,
    ) -> None:
        """Links the posts corresponding to thread thread_root.

        thread_root must be a child of the root of the entire tree.
        """

        # Do DFS to get the order of traversal.
        post_stack = []
        for child in reversed(thread_root.children):
            post_stack.append(child)
        visited = []
        while len(post_stack):
            current_post = post_stack.pop()
            visited.append(current_post)
            for child in reversed(current_post.children):
                post_stack.append(child)

        # Link the posts to the threads and to each other.
        visited[0].prev_post_ref_id = thread_root.get_id()
        thread_root.next_post_ref_id = visited[0].get_id()
        for i in range(len(visited)):
            post = visited[i]
            if prev_thread:
                post.prev_thread_ref_id = prev_thread.get_id()
            if next_thread:
                post.next_thread_ref_id = next_thread.get_id()
            next_post = visited[i + 1] if i <= len(visited) - 2 else None
            if next_post:
                post.next_post_ref_id = next_post.get_id()
                next_post.prev_post_ref_id = post.get_id()
        if next_thread:
            visited[-1].next_post_ref_id = next_thread.get_id()
            next_thread.prev_post_ref_id = visited[-1].get_id()

    def _build_tree(self, nodes: list[ThreadForestNode]) -> ThreadForestNode:
        tree_root = ThreadForestNode(-1, [])

        # Dict of message_id to ThreadForestNode
        message_id_map: dict[str, ThreadForestNode] = dict()

        # Add nodes to the dict.
        for node in nodes:
            message_id = node.message_id
            if message_id:
                message_id_map[message_id] = node

        # Traverse again to add parent-child relations.
        for node in nodes:
            in_reply_to = node.in_reply_to
            if in_reply_to:
                # This message is a reply of some message.
                parent_node = message_id_map.get(in_reply_to)
                if parent_node:
                    parent_node.children.append(node)
                else:
                    # Unable to find parent. Add to root.
                    tree_root.children.append(node)
            else:
                # This message isn't a reply of any message.
                # Add it to the root.
                tree_root.children.append(node)

        # Walk the tree again to set the last several links properly.
        # Start with the children of the root node, which represent threads.
        for i in range(len(tree_root.children)):
            thread = tree_root.children[i]
            next_thread = (
                tree_root.children[i + 1]
                if i <= len(tree_root.children) - 2
                else None
            )
            if next_thread:
                thread.next_thread_ref_id = next_thread.get_id()
                next_thread.prev_thread_ref_id = thread.get_id()
            if len(thread.children):
                prev_thread = tree_root.children[i - 1] if i >= 1 else None
                self._link_posts_of(thread, prev_thread, next_thread)
            elif next_thread:
                thread.next_post_ref_id = next_thread.get_id()
                next_thread.prev_post_ref_id = thread.get_id()

        return tree_root

    def get_root(self) -> ThreadForestNode:
        return self._tree

    def to_dict(self) -> dict[str, Any]:
        return self._tree.to_dict()

    def serialize(self) -> str:
        serialized = ""
        for child in self._tree.children:
            serialized += child.serialize(0)
        return serialized
