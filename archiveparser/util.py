def escape(message: str) -> str:
    return (
        message.replace("{", "&#123;")
        .replace("}", "&#125;")
        .replace("[", "&#91;")
        .replace("]", "&#93;")
        .replace('"', "&quot;")
        .replace(":", "&#58;")
        .replace("_", "&#95;")
        .replace("*", "&#42;")
        .replace("<", "&lt;")
        .replace(">", "&gt;")
        .replace("~", "&#126;")
        .replace("|", "&#124;")
        .replace("^", "&#94;")
    )


def pad_id(ref_id: int) -> str:
    return f"{ref_id:04d}"
