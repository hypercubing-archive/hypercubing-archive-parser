# The following program generates the archive files
# given the mbox files.

from email.parser import BytesParser
from email.policy import SMTP
import json
import sys

from archiveparser.archive_message_tree import ArchiveMessageTree
from archiveparser.archive_writer import ArchiveWriter
from archiveparser.mbox_splitter import split_messages
from archiveparser.thread_forest import ThreadForest, ThreadForestNode
from archiveparser.util import pad_id


def main() -> None:
    # Concatenate the mailboxes.
    combined_mailboxes = b""
    for mbox_number in range(1, 5):
        with open(f"mbox/10714925.mbox.0000{mbox_number}", "rb") as f:
            combined_mailboxes += f.read()

    # Split the messages.
    messages = split_messages(combined_mailboxes)

    # Parse the messages.
    message_parser = BytesParser(policy=SMTP)
    message_trees = []
    for i, message in enumerate(messages):
        parsed_message = message_parser.parsebytes(message)

        # Don't include deleted messages
        if "X-Deleted-Message" in parsed_message:
            continue

        # A single message is actually a tree
        message_tree = ArchiveMessageTree(i + 1, parsed_message)  # type:ignore
        message_trees.append(message_tree)

    # Generate the nodes for the thread forest
    thread_forest_nodes = []
    for tree in message_trees:
        header = tree.get_header()
        ref_id = tree.get_id()
        from_name_email = header["From"]
        subject = header["Subject"]
        date = header["Date"]
        message_id = header["Message-ID"]
        in_reply_to = header["In-Reply-To"]
        thread_forest_node = ThreadForestNode(
            ref_id, [], from_name_email, subject, date, message_id, in_reply_to
        )
        thread_forest_nodes.append(thread_forest_node)

    # Generate the thread forest.
    thread_forest = ThreadForest(thread_forest_nodes)

    # Generate the JSON file for reference.
    with open("intermediate/thread_tree.json", "w") as f:
        json.dump(thread_forest.to_dict(), f, indent=4)

    # Generate the archive
    archive_writer = ArchiveWriter(message_trees, thread_forest)
    archive = archive_writer.generate_archive()

    # Save the files
    with open("intermediate/threads.md", "w") as f:
        f.write(archive.index_page)

    assert len(message_trees) == len(archive.posts)
    for post in archive.posts:
        ref_id = post.get_id()
        with open(f"intermediate/messages/{pad_id(ref_id)}.md", "w") as f:
            f.write(post.get_content())


if __name__ == "__main__":
    main()
